package es.plexus.cedei.heroes.controllers;

import es.plexus.cedei.heroes.exceptions.HeroesException;
import es.plexus.cedei.heroes.model.Personaje;
import es.plexus.cedei.heroes.services.PersonajeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class HeroesController {

  private PersonajeService personajeService;


  @Autowired(required = true)
  @Qualifier(value = "personajeService")
  public void setPersonajeService(PersonajeService ps){
    this.personajeService = ps;
  }

  @RequestMapping(value = "/api/heroe", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public String crearHeroe() {

    return "crearHeroe";

  }

  @RequestMapping(value = "/api/generarError", method = RequestMethod.GET)
  public void generarError() {
    throw new HeroesException("Generamos el error", "GENERATED_ERROR",
        "Esto es un error generado tras pulsar el boton");
  }


  @RequestMapping(value = "/api/heroe", method = RequestMethod.POST, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
  @ResponseBody
  public ResponseEntity<Personaje> postHeroe(@RequestBody() Personaje p) {
    Personaje personaje = new Personaje();
    System.out.println("personaje " + p);
    this.personajeService.addPersonaje(p);
    personaje.setId(p.getId());
    personaje.setBio(p.getBio());
    personaje.setNombre(p.getNombre());
    personaje.setTipo(p.getTipo());
    personaje.setFecha_aparicion(p.getFecha_aparicion());
    personaje.setImagen(p.getImagen());

    return new ResponseEntity<Personaje>(personaje, HttpStatus.OK);

  }

  @RequestMapping(value = "/api/heroe/{id}", method = RequestMethod.GET, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
  @ResponseBody
  public ResponseEntity<Personaje> obtenerHeroe(@PathVariable("id") Long id) {
    Personaje result = this.personajeService.getPersonaje(id);
    Personaje p = new Personaje(result.getId(),
                                result.getNombre(),
                                result.getTipo(),
                                result.getBio(),
                                result.getFecha_aparicion(),
                                result.getImagen());

    return new ResponseEntity<>(p, HttpStatus.OK);
  }

  @RequestMapping(value = "/api/heroes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  @ResponseBody
  public ResponseEntity<List<Personaje>> obtenerHeroes() {
    ArrayList<Personaje> listaH = new ArrayList<>();
    for (Personaje heroe: this.personajeService.listaPersonajes()) {
      listaH.add(new Personaje(heroe.getId(),
                                heroe.getNombre(),
                                heroe.getTipo(),
                                heroe.getBio(),
                                heroe.getFecha_aparicion(),
                                heroe.getImagen()));
    }
    return new ResponseEntity<>(listaH, HttpStatus.OK);
  }


  @RequestMapping(value = "/api/heroe/{id}", method = RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
  @ResponseBody
  public ResponseEntity<Personaje> eliminarHeroe(@PathVariable("id") Long id) {
    Personaje result = this.personajeService.getPersonaje(id);
    Personaje p = new Personaje(id,
                                result.getNombre(),
                                result.getTipo(),
                                result.getBio(),
                                result.getFecha_aparicion(),
                                result.getImagen());
    this.personajeService.removePersonaje(id);
    return new ResponseEntity<>(p, HttpStatus.OK);
  }


  @RequestMapping(value = "/api/heroe/{id}", method = RequestMethod.PUT, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
  public ResponseEntity<Personaje> actualizarHeroe(@PathVariable("id") Long id,
                                                   @RequestBody() Personaje personaje) {

    Personaje p = new Personaje(id,
                                personaje.getNombre(),
                                personaje.getTipo(),
                                personaje.getBio(),
                                personaje.getFecha_aparicion(),
                                personaje.getImagen());
    this.personajeService.updatePersonaje(p);
    return new ResponseEntity<Personaje>(p, HttpStatus.OK);
  }

  @RequestMapping(value = "/api/heroes/null", method = RequestMethod.GET, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
  @ResponseBody
  public ResponseEntity<String> noPersonaje(@PathVariable("id") Long id) {
        return new ResponseEntity<>("No Existe ningún Personaje con el ID proporcionado.", HttpStatus.OK);
  }

}