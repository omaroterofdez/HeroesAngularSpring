package es.plexus.cedei.heroes.exceptions;

import org.springframework.http.HttpStatus;

/**
 * Created by cedei06 on 27/07/2017.
 */
public class HeroesException extends RuntimeException {

  private final String codigoError;
  private final HttpStatus httpStatus;
  private final String mensajeUsuario;

  public HeroesException(String message, String codigoError, String mensajeUsuario) {
    super(message);
    this.codigoError = codigoError;
    this.mensajeUsuario = mensajeUsuario;
    this.httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
  }

  public HeroesException(String message, Throwable cause, String codigoError, String mensajeUsuario) {
    super(message, cause);
    this.codigoError = codigoError;
    this.mensajeUsuario = mensajeUsuario;
    this.httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
  }

  public HeroesException(String message, Throwable cause, String codigoError, HttpStatus httpStatus, String mensajeUsuario) {
    super(message, cause);
    this.codigoError = codigoError;
    this.httpStatus = httpStatus;
    this.mensajeUsuario = mensajeUsuario;
  }

  public String getCodigoError() {
    return codigoError;
  }

  public HttpStatus getHttpStatus() {
    return httpStatus;
  }

  public String getMensajeUsuario() {
    return mensajeUsuario;
  }
}
