package es.plexus.cedei.heroes.interceptors;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created by cedei06 on 25/07/2017.
 */
public class LoginInterceptor extends HandlerInterceptorAdapter {
  private static final Logger log = Logger.getLogger(LoginInterceptor.class);

  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
    log.debug("[CEDEI-HEROES] preHandle IN");
    HttpSession session = request.getSession(true);

    if (usuarioAutenticado(session.getAttribute("usuario"))) {
      log.debug("[CEDEI-HEROES] preHandle OUT - Usuario autenticado OK");
      return true;
    }

    log.debug("[CDEI-HEROES] preHandle OUT - Usuario no autenticado REDIRECT LOGIN");
    response.sendRedirect(request.getServletPath() + "/login");

    return false;
  }

  private boolean usuarioAutenticado(Object usuario) {
    return usuario != null && !((String) usuario).isEmpty();
  }
}
