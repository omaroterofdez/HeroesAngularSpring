package es.plexus.cedei.heroes.model;

/**
 * Created by cedei06 on 27/07/2017.
 */
public class ErrorInfo {
  private String codigo;
  private String mensaje;

  public ErrorInfo(String codigo, String mensaje) {
    this.codigo = codigo;
    this.mensaje = mensaje;
  }

  public String getCodigo() {
    return codigo;
  }

  public void setCodigo(String codigo) {
    this.codigo = codigo;
  }

  public String getMensaje() {
    return mensaje;
  }

  public void setMensaje(String mensaje) {
    this.mensaje = mensaje;
  }
}
