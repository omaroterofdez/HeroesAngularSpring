package es.plexus.cedei.heroes.exceptions;

import es.plexus.cedei.heroes.model.ErrorInfo;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.ejb.ObjectNotFoundException;

/**
 * Created by cedei06 on 27/07/2017.
 */
@ControllerAdvice
public class HeroesExceptionHandler {

  private static final Logger log = Logger.getLogger(HeroesExceptionHandler.class);

  @ExceptionHandler(HeroesException.class)
  @ResponseBody
  public ResponseEntity<ErrorInfo> handleHeroesException(HeroesException e) {
    log.error("[CEDEI-HEROES] Se ha producido un error: " + e.getMessage(), e);

    ErrorInfo errorInfo = new ErrorInfo(e.getCodigoError(), e.getMensajeUsuario());

    return new ResponseEntity<>(errorInfo, e.getHttpStatus());
  }
}
