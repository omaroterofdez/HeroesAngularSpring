package es.plexus.cedei.heroes.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by cedei06 on 24/07/2017.
 */
@Entity
@Table(name="personajes")
public class Personaje {

  @Id
  @Column(name="id")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  @Column(name="nombre", nullable = false)
  private String nombre;
  @Column(name="tipo", nullable = false)
  private String tipo;
  @Column(name="bio")
  private String bio;
  @Column(name="fecha_aparicion")
  @Temporal(TemporalType.DATE)
  private Date fecha_aparicion;
  @Column(name="imagen")
  private String imagen;

  public Personaje(Long id, String nombre, String tipo, String bio, Date fecha_aparicion, String imagen) {
    this.id = id;
    this.nombre = nombre;
    this.tipo = tipo;
    this.bio = bio;
    this.fecha_aparicion = fecha_aparicion;
    this.imagen = imagen;
  }

  public Date getFecha_aparicion() {
    return fecha_aparicion;
  }

  public void setFecha_aparicion(Date fecha_aparicion) {
    this.fecha_aparicion = fecha_aparicion;
  }

  public String getImagen() {
    return imagen;
  }

  public void setImagen(String imagen) {
    this.imagen = imagen;
  }

  public Personaje() {

  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public String getTipo() {
    return tipo;
  }

  public void setTipo(String tipo) {
    this.tipo = tipo;
  }

  public String getBio() {
    return bio;
  }

  public void setBio(String bio) {
    this.bio = bio;
  }

}
