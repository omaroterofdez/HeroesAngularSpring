<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>Creación de héroes</title>
  </head>
  <body>
    Usuario: ${usuario}
    <br><br>

    <form action="/heroes/heroe" method="POST">
      <label for="nombre">Nombre:</label>
      <br>
      <input id="nombre" name="nombre" type="text"/>
      <br><br>
      <label for="tipo">Tipo:</label>
      <br>
      <select id="tipo" name="tipo">
        <option value="heroe">Heroe</option>
        <option value="villano">Villano</option>
      </select>
      <br><br>
      <label for="bio">Bio:</label>
      <br>
      <textarea id="bio" name="bio"></textarea>
      <br><br>
      <button type="submit">Crear Héroe</button>
      <form action="/heroes/logout" method="POST">
            <button type="submit">Logout</button>
    </form>
  </body>
</html>