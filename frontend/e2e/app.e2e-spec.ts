import { FrontendSuperheroesPage } from './app.po';

describe('frontend-superheroes App', () => {
  let page: FrontendSuperheroesPage;

  beforeEach(() => {
    page = new FrontendSuperheroesPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
