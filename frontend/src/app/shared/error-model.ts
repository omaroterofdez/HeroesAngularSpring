export class ErrorModel {
 public codigo: string;
 public mensaje: string;

 constructor(codigo: string, mensaje: string) {
   this.codigo = codigo;
   this.mensaje = mensaje;
 }
}
