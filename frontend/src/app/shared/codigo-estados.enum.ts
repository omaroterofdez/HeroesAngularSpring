export enum CodigoEstadosEnum {
   estadoInicial,
   enviando,
   envioOkPost,
   envioOkPut,
   envioOkGet,
   envioError,
   formValido,
   formInvalido
}
