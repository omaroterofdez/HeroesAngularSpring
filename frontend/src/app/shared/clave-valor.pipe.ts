import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'claveValor'
})
export class ClaveValorPipe implements PipeTransform {

  transform(value: any, ejecutarPipe: boolean, args?: any): any {
    let keyValue: {
      key: string;
      value: string;
    }[] = [];

    for (let key in value){
      keyValue.push({
        key: key,
        value: value[key]
      });
    }
      return keyValue;
  }

}
