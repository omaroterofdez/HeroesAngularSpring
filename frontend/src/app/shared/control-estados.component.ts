import { CodigoEstadosEnum } from "./codigo-estados.enum";
import { ErrorModel } from "./error-model";

export class ControlEstados {
 private estadoActual: CodigoEstadosEnum;
 private listaErrores: ErrorModel[];

 constructor() {
   this.estadoActual = CodigoEstadosEnum.estadoInicial;
   this.listaErrores = [];
 }

/**
* Limpia el estado actual dejandolo a CodigoEstadosEnum.estadoInicial y vaciando la lista de errores
*/
  public limpiarEstado() {
   this.estadoActual = CodigoEstadosEnum.estadoInicial;
   if (this.listaErrores != null) {
     this.listaErrores.splice(0, this.listaErrores.length);
   }
  }

  /**
  * Agrega un errorModel (codigo de error + mensaje) a la lista de errores y fija el estado actual a
  * CodigoEstadosEnum.envioError
  * @param {ErrorModel} errorModel
  */
  public addError(errorModel: ErrorModel) {
   this.estadoActual = CodigoEstadosEnum.envioError;
   this.listaErrores.push(errorModel);
  }

  /**
  * Establece el estado actual. Las reglas son:
  * - No se puede utilizar este metodo para cambiar el estado a CodigoEstadosEnum.envioError
  * - Si el estado actual es CodigoEstadosEnum.envioError tampoco puede ser cambiado
  * - Utilizar el metodo addError() para fijar el estado actual a CodigoEstadosEnum.envioError y
  * agregar un ErrorModel
  * @param {CodigoEstadosEnum} estadoNuevo Estado nuevo a establecer
  */
  public setEstadoActual(estadoNuevo: CodigoEstadosEnum) {
   if (this.estadoActual !== CodigoEstadosEnum.envioError && estadoNuevo !== CodigoEstadosEnum.envioError) {
     this.estadoActual = estadoNuevo;
   }
  }

  /**
  * Devuelve el estado actual fijado
  * @returns {CodigoEstadosEnum} Enumerado del estado actual fijado
  */
  public getEstadoActual(): CodigoEstadosEnum {
   return this.estadoActual;
  }

  /**
  * Devuelve el listado de los errores registrados
  * @returns {ErrorModel[]} Array con los errores registrados
  */
  public getListaErrores(): ErrorModel[] {
   return this.listaErrores;
  }

  /**
  * Nos indica si el estado actual es de error
  * @returns {boolean} true si el estado actual es error
  */
  public isError(): boolean {
   return this.estadoActual === CodigoEstadosEnum.envioError;
  }
}
