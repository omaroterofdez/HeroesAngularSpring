import { Injectable } from '@angular/core';

import { Location } from '@angular/common'

import { Router } from '@angular/router';

import { PersonajeModel } from './PersonajeForm.model';
import { Headers, Http, Response } from '@angular/http';
import 'rxjs/Rx';

@Injectable()
export class PersonajeService {

  private obtenPersonajeURL: string="http://localhost:8080/heroes/api/heroe/";

  private personajesURL: string = "http://localhost:8080/heroes/api/heroes";

  public listaPersonajes: PersonajeModel[] = [];
  public listaHeroes: PersonajeModel[] = [];
  public listaVillanos: PersonajeModel[] = [];

  public list: PersonajeModel[];

  public tempUrl: string;

  constructor(private _http: Http, private _router: Router, private _location: Location) {}

  public nuevoPersonaje(personaje: PersonajeModel){
    let body: string =  JSON.stringify(personaje);
    let headers: Headers = new Headers({
      "Content-type": "application/json"
    });
      return this._http.post(this.obtenPersonajeURL, body, {headers: headers })
        .map((res: Response)  => {
          return res;
      })
  }

  public actualizarPersonaje(personaje: PersonajeModel, key:number){
    let body: string =  JSON.stringify(personaje);
    let headers: Headers = new Headers({
      "Content-type": "application/json"
    });
    let url=`${this.obtenPersonajeURL}${key}.json`;
      return this._http.put( url , body, {headers: headers })
        .map((res: Response)  => {
          return res.json();
      })
  }

  public obtenerPersonaje(key: any){
    let headers: Headers = new Headers({
      "Content-type": "application/json"
    });
    let url=`${this.obtenPersonajeURL}${key}.json`;
      return this._http.get( url , {headers: headers })
        .map((res: Response)  => {
          return res.json();
      })
  }

  public obtenerPersonajes(){
    this.listaPersonajes = [];
    this.listaHeroes = [];
    this.listaVillanos = [];
    this.tempUrl = this._router.url;
    let headers: Headers = new Headers({
      "Content-type": "application/json"
    });
    console.log(this.personajesURL)
    let url=`${this.personajesURL}`;
      return this._http.get( url , {headers: headers })
        .map((res: Response)  => {
          this.listaPersonajes = res.json();
          return res.json();
      })
  }

  public filtrarTipo(){
    for(let personaje of this.listaPersonajes){
      if(personaje.tipo == 'heroe'){
        this.listaHeroes.push(personaje)
      }else{
        this.listaVillanos.push(personaje)
      }
    }
  }


  public borrarPersonaje(key: number){
    let headers: Headers = new Headers({
      "Content-type": "application/json"
    });
    let url=`${this.obtenPersonajeURL}${key}.json`;
      return this._http.delete( url , {headers: headers })
        .map((res: Response)  => {
          return res.json();
      })
  }

  public buscarPersonaje(buscarTexto: string){
    this.list = [];
    for (let personaje of this.listaPersonajes) {
      let name: string = personaje.nombre;
      if(name.toLowerCase().includes(buscarTexto.toLowerCase())){
        this.list.push(personaje);
      }
    }
    return this.list;
  }

  public volver(){
    this._location.back();
  }

}
