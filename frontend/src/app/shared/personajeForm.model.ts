export class PersonajeModel{
  nombre: string;
  tipo: string;
  bio: string;
  imagen: string;
  fecha_aparicion: Date;

  constructor(){
    this.nombre = "";
    this.tipo= null;
    this.bio= "";
    this.fecha_aparicion = null;
    this.imagen= "";
  }
}
