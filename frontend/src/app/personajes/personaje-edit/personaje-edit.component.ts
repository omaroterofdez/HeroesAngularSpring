import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute} from '@angular/router';
import { Location } from '@angular/common';
import { EstadoFormEnum } from '../../shared/estadoForm.enum';

import { NavbarComponent } from '../../navbar/navbar.component';

import { Observable } from 'rxjs/Observable';
import { PersonajeService} from '../../shared/personaje-service';

import { PersonajeModel } from '../../shared/personajeForm.model';
import { FirebasePostResponseModel } from '../../shared/firebase-response.model';

@Component({
  selector: 'heroes-api-personaje-edit',
  templateUrl: './personaje-edit.component.html'
})
export class PersonajeEditComponent implements OnInit {

  public estadoFormEnum = EstadoFormEnum;
  public estadoForm: EstadoFormEnum = EstadoFormEnum.estadoInicial;

  public heroeForm: FormGroup = new FormGroup({
    "nombre": new FormControl("", Validators.required),
    "tipo": new FormControl("", Validators.required),
    "bio": new FormControl(""),
    "fecha_aparicion": new FormControl("", [Validators.required]),
    "imagen": new FormControl(null)
  })

  public id;

  public personaje : PersonajeModel;

  constructor(private _activatedRoute: ActivatedRoute,
              private _router: Router,
              private _personajeService: PersonajeService,
              private _location: Location) { }

  ngOnInit(){
    this.heroeForm.reset(this.personaje);
    this._activatedRoute.params
     .switchMap( params => {
       this.id = params['id'];
       if(this.id=='nuevo'){
         Observable.of(new PersonajeModel()).subscribe(datos =>{
          this.heroeForm.reset(datos)}
        );
       }else{
         return this._personajeService.obtenerPersonaje(this.id);
       }
     })
  }

  public verImagen(imagen){
    return imagen.url;
  }

  public addUpdate(){
    if(this.heroeForm.valid){
      this.estadoForm = EstadoFormEnum.enviando;
      if(this.heroeForm.value.imagen == null || this.heroeForm.value.imagen == null ){
        this.heroeForm.value.imagen = "assets/img/no-image.png";
      };
      if (this.id == 'nuevo'){
        this.altaPersonaje();
      }else{
        this.actualizarPersonaje();
      }
    }else{
      this.estadoForm = EstadoFormEnum.formInvalido;
    }
  }

public altaPersonaje(){
  this._personajeService.nuevoPersonaje(this.heroeForm.value)
    .subscribe( (datos) =>{
      this.estadoForm = EstadoFormEnum.envioOkPut;
    },
  error => {
    console.error("Error al insertar Personaje", error);
    this.estadoForm = EstadoFormEnum.envioError;
  })
}

  public actualizarPersonaje(){
    this._personajeService.actualizarPersonaje(this.heroeForm.value, this.id)
      .subscribe( (datos: PersonajeModel) =>{
        this.estadoForm = EstadoFormEnum.envioOkPut;
      },
    error => {
      console.error("Error al actualizar Personaje", error);
      this.estadoForm = EstadoFormEnum.envioError;
    })
  }

  public volver(){
    this._personajeService.volver();
  }
}
