import { Component, OnInit} from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';

import { PersonajeService} from '../../shared/personaje-service';

import { PersonajeModel } from '../../shared/personajeForm.model';

@Component({
  selector: 'heroes-api-villanos',
  templateUrl: './villanos.component.html'
})
export class VillanosComponent implements OnInit {

  public listaVillanos: PersonajeModel[] = [];

  constructor(private _activatedRoute: ActivatedRoute,
              private _router: Router,
              private _personajeService: PersonajeService) { }

  ngOnInit() {
    this._personajeService.obtenerPersonajes()
    .subscribe(() => {
        this._personajeService.filtrarTipo();
        this.listaVillanos = this._personajeService.listaVillanos;
    })
  }

  public borrarVillano(key: number){
    this._personajeService.borrarPersonaje(key)
      .subscribe(res => {
        if (res != null){
          delete this.listaVillanos[key];
        }else{
          console.error("Error al eliminar Villano res", res)
        }
      }, error => {
        console.error("Error al eliminar Villano", error);
      });
  }

  public editVillano(key: string){
    this._router.navigate(['/personaje/edit', key]);
  }

  public verDetalle(key: string){
    this._router.navigate(['/personaje', key]);
  }
}
