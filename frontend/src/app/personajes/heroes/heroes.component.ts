import { Component, OnInit} from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';

import { PersonajeService} from '../../shared/personaje-service';

import { PersonajeModel } from '../../shared/personajeForm.model';

@Component({
  selector: 'heroes-api-heroes',
  templateUrl: './heroes.component.html'
})
export class HeroesComponent implements OnInit{

  public listaHeroes: PersonajeModel[] = [];

  constructor(private _activatedRoute: ActivatedRoute,
              private _router: Router,
              private _personajeService: PersonajeService) { }

  ngOnInit() {
      this._personajeService.obtenerPersonajes()
      .subscribe(() => {
        this._personajeService.filtrarTipo();
        this.listaHeroes = this._personajeService.listaHeroes;
      })
  }

  public borrarHeroe(key: number){
    this._personajeService.borrarPersonaje(key)
      .subscribe(res => {
        if (res != null){
          delete this.listaHeroes[key];
        }else{
          console.error("Error al eliminar Heroe res", res)
        }
      }, error => {
        console.error("Error al eliminar Heroe", error);
      });
  }

  public editHeroe(key: number){
    this._router.navigate(['/personaje/edit', key]);
  }

  public verDetalle(key: number){
    this._router.navigate(['/personaje', key]);
  }
}
