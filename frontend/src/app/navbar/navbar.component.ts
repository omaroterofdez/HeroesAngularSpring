import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';


import { PersonajeService} from '../shared/personaje-service';

@Component({
  selector: 'heroes-api-navbar',
  templateUrl: './navbar.component.html'
})
export class NavbarComponent implements OnInit {

  constructor(private _activatedRoute: ActivatedRoute,
              private _router: Router,
              private _personajeService: PersonajeService) { }

  ngOnInit() {
  }

  public buscarHeroe(buscarTexto:string){
    this._router.navigate(['search', buscarTexto]);
  }

}
