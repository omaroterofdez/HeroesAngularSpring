import { Component, OnInit } from '@angular/core';

import { PersonajeService} from './../shared/personaje-service';

@Component({
  selector: 'heroes-api-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {

  constructor(private personajeService: PersonajeService) { }

  ngOnInit() {

    if(this.personajeService.listaPersonajes.length == 0 || !this.personajeService.listaPersonajes){
      this.personajeService.obtenerPersonajes();
    }

  }

}
