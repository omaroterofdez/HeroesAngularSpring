import { Component, OnInit } from '@angular/core';


import { Router, ActivatedRoute} from '@angular/router';

import { PersonajeService } from '../shared/personaje-service';

import { PersonajeModel } from '../shared/personajeForm.model';

@Component({
  selector: 'heroes-api-search',
  templateUrl: './search.component.html'
})
export class SearchComponent implements OnInit {

  private lista: PersonajeModel[];

    constructor (private activatedRoute: ActivatedRoute, private personajeService: PersonajeService, private router: Router){ }

    ngOnInit(){

      if(this.personajeService.listaPersonajes.length == 0 || !this.personajeService.listaPersonajes){
        this.personajeService.obtenerPersonajes().subscribe(list =>{
          this.lista = list;
        })
      }
      this.activatedRoute.params.subscribe(params => {
        this.lista = this.personajeService.buscarPersonaje(params['buscarTexto']);
      })
    }

    public verDetalle(id: number){
      this.router.navigate(['/personaje', id]);
    }
}
